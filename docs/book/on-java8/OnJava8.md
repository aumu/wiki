## 第一章 对象的概念

  * [第一章 对象的概念](/book/on-java8/01-What-is-an-Object.md)
  * [抽象](/book/on-java8/01-What-is-an-Object.md)
  * [接口](/book/on-java8/01-What-is-an-Object.md)
  * [服务提供](/book/on-java8/01-What-is-an-Object.md)
  * [封装](/book/on-java8/01-What-is-an-Object.md)
  * [复用](/book/on-java8/01-What-is-an-Object.md)
  * [继承](/book/on-java8/01-What-is-an-Object.md)
  * [多态](/book/on-java8/01-What-is-an-Object.md)
  * [单继承](/book/on-java8/01-What-is-an-Object.md)
  * [集合](/book/on-java8/01-What-is-an-Object.md)
  * [生命周期](/book/on-java8/01-What-is-an-Object.md)
  * [异常处理](/book/on-java8/01-What-is-an-Object.md)
  * [本章小结](/book/on-java8/01-What-is-an-Object.md)
  
## 第二章 安装Java和本书用例
  
  * [第二章 安装Java和本书用例](/book/on-java8/02-Installing-Java-and-the-Book-Examples.md)
  * [编辑器](/book/on-java8/02-Installing-Java-and-the-Book-Examples.md)
  * [Shell](/book/on-java8/02-Installing-Java-and-the-Book-Examples.md)
  * [Java安装](/book/on-java8/02-Installing-Java-and-the-Book-Examples.md)
  * [校验安装](/book/on-java8/02-Installing-Java-and-the-Book-Examples.md)
  * [安装和运行代码示例](/book/on-java8/02-Installing-Java-and-the-Book-Examples.md)

## 第三章 万物皆对象

  * [第三章 万物皆对象](/book/on-java8/03-Objects-Everywhere.md)
  * [对象操纵](/book/on-java8/03-Objects-Everywhere.md)
  * [对象创建](/book/on-java8/03-Objects-Everywhere.md)
  * [代码注释](/book/on-java8/03-Objects-Everywhere.md)
  * [对象清理](/book/on-java8/03-Objects-Everywhere.md)
  * [类的创建](/book/on-java8/03-Objects-Everywhere.md)
  * [程序编写](/book/on-java8/03-Objects-Everywhere.md)
  * [小试牛刀](/book/on-java8/03-Objects-Everywhere.md)
  * [编码风格](/book/on-java8/03-Objects-Everywhere.md)
  * [本章小结](/book/on-java8/03-Objects-Everywhere.md)

## 第四章 运算符 

  * [第四章 运算符](/book/on-java8/04-Operators.md)
  * [开始使用](/book/on-java8/04-Operators.md)
  * [优先级](/book/on-java8/04-Operators.md)
  * [赋值](/book/on-java8/04-Operators.md)
  * [算术运算符](/book/on-java8/04-Operators.md)
  * [递增和递减](/book/on-java8/04-Operators.md)
  * [关系运算符](/book/on-java8/04-Operators.md)
  * [逻辑运算符](/book/on-java8/04-Operators.md)
  * [字面值常量](/book/on-java8/04-Operators.md)
  * [按位运算符](/book/on-java8/04-Operators.md)
  * [移位运算符](/book/on-java8/04-Operators.md)
  * [三元运算符](/book/on-java8/04-Operators.md)
  * [字符串运算符](/book/on-java8/04-Operators.md)
  * [常见陷阱](/book/on-java8/04-Operators.md)
  * [类型转换](/book/on-java8/04-Operators.md)
  * [Java没有sizeof](/book/on-java8/04-Operators.md)
  * [运算符总结](/book/on-java8/04-Operators.md)
  * [本章小结](/book/on-java8/04-Operators.md)

## 第五章 控制流
  
  * [第五章 控制流](/book/on-java8/05-Control-Flow.md)
  * [true和flase](/book/on-java8/05-Control-Flow.md)
  * [if-else](/book/on-java8/05-Control-Flow.md)
  * [迭代语句](/book/on-java8/05-Control-Flow.md)
  * [for-in语法](/book/on-java8/05-Control-Flow.md)
  * [return](/book/on-java8/05-Control-Flow.md)
  * [break和continue](/book/on-java8/05-Control-Flow.md)
  * [臭名昭著的goto](/book/on-java8/05-Control-Flow.md)
  * [switch](/book/on-java8/05-Control-Flow.md)
  * [switch字符串](/book/on-java8/05-Control-Flow.md)
  * [本章小结](/book/on-java8/05-Control-Flow.md)
  
## 第六章 初始化和清理
  
  * [第六章 初始化和清理](/book/on-java8/06-Housekeeping.md)
  * [利用构造器保证初始化](/book/on-java8/06-Housekeeping.md)
  * [方法重载](/book/on-java8/06-Housekeeping.md)
  * [无参构造器](/book/on-java8/06-Housekeeping.md)
  * [this关键字](/book/on-java8/06-Housekeeping.md)
  * [垃圾回收器](/book/on-java8/06-Housekeeping.md)
  * [成员初始化](/book/on-java8/06-Housekeeping.md)
  * [构造器初始化](/book/on-java8/06-Housekeeping.md)
  * [数组初始化](/book/on-java8/06-Housekeeping.md)
  * [枚举类型](/book/on-java8/06-Housekeeping.md)
  * [本章小结](/book/on-java8/06-Housekeeping.md)
  
## 第七章 封装

  * [第七章 封装](/book/on-java8/07-Implementation-Hiding.md)
  * [包的概念](/book/on-java8/07-Implementation-Hiding.md)
  * [访问权限修饰符](/book/on-java8/07-Implementation-Hiding.md)
  * [接口和实现](/book/on-java8/07-Implementation-Hiding.md)
  * [类访问权限](/book/on-java8/07-Implementation-Hiding.md)
  * [本章小结](/book/on-java8/07-Implementation-Hiding.md)
  
## 第八章 复用
  
  * [第八章 复用](/book/on-java8/08-Reuse.md)
  * [组合语法](/book/on-java8/08-Reuse.md)
  * [继承语法](/book/on-java8/08-Reuse.md)
  * [委托](/book/on-java8/08-Reuse.md)
  * [结合组合与继承](/book/on-java8/08-Reuse.md)
  * [组合与继承的选择](/book/on-java8/08-Reuse.md)
  * [protected](/book/on-java8/08-Reuse.md)
  * [向上转型](/book/on-java8/08-Reuse.md)
  * [final关键字](/book/on-java8/08-Reuse.md)
  * [类初始化和加载](/book/on-java8/08-Reuse.md)
  * [本章小结](/book/on-java8/08-Reuse.md)
  
## 第九章 多态
  
  * [第九章 多态](/book/on-java8/09-Polymorphism.md)
  * [向上转型回溯](/book/on-java8/09-Polymorphism.md)
  * [深入理解](/book/on-java8/09-Polymorphism.md)
  * [构造器和多态](/book/on-java8/09-Polymorphism.md)
  * [返回类型协变](/book/on-java8/09-Polymorphism.md)
  * [使用继承设计](/book/on-java8/09-Polymorphism.md)
  * [本章小结](/book/on-java8/09-Polymorphism.md)

## 第十章 接口

  * [第十章 接口](/book/on-java8/10-Interfaces.md)
  * [抽象类和方法](/book/on-java8/10-Interfaces.md)
  * [接口创建](/book/on-java8/10-Interfaces.md)
  * [抽象类和接口](/book/on-java8/10-Interfaces.md)
  * [完全解耦](/book/on-java8/10-Interfaces.md)
  * [多接口结合](/book/on-java8/10-Interfaces.md)
  * [使用继承扩展接口](/book/on-java8/10-Interfaces.md)
  * [接口适配](/book/on-java8/10-Interfaces.md)
  * [接口字段](/book/on-java8/10-Interfaces.md)
  * [接口嵌套](/book/on-java8/10-Interfaces.md)
  * [接口和工厂方法模式](/book/on-java8/10-Interfaces.md)
  * [本章小结](/book/on-java8/10-Interfaces.md)
  
## 第十一章 内部类
  
  * [第十一章 内部类](/book/on-java8/11-Inner-Classes.md)
  * [创建内部类](/book/on-java8/11-Inner-Classes.md)
  * [链接外部类](/book/on-java8/11-Inner-Classes.md)
  * [内部类this和new的使用](/book/on-java8/11-Inner-Classes.md)
  * [内部类向上转型](/book/on-java8/11-Inner-Classes.md)
  * [内部类方法和作用域](/book/on-java8/11-Inner-Classes.md)
  * [匿名内部类](/book/on-java8/11-Inner-Classes.md)
  * [嵌套类](/book/on-java8/11-Inner-Classes.md)
  * [为什么需要内部类](/book/on-java8/11-Inner-Classes.md)
  * [继承内部类](/book/on-java8/11-Inner-Classes.md)
  * [重写内部类](/book/on-java8/11-Inner-Classes.md)
  * [内部类局部变量](/book/on-java8/11-Inner-Classes.md)
  * [内部类标识符](/book/on-java8/11-Inner-Classes.md)
  * [本章小结](/book/on-java8/11-Inner-Classes.md)
  
## 第十二章 集合

  * [第十二章 集合](/book/on-java8/12-Collections.md)
  * [泛型和类型安全的集合](/book/on-java8/12-Collections.md)
  * [基本概念](/book/on-java8/12-Collections.md)
  * [添加元素组](/book/on-java8/12-Collections.md)
  * [集合的打印](/book/on-java8/12-Collections.md)
  * [列表List](/book/on-java8/12-Collections.md)
  * [迭代器Iterators](/book/on-java8/12-Collections.md)
  * [链表LinkedList](/book/on-java8/12-Collections.md)
  * [堆栈Stack](/book/on-java8/12-Collections.md)
  * [集合Set](/book/on-java8/12-Collections.md)
  * [映射Map](/book/on-java8/12-Collections.md)
  * [队列Queue](/book/on-java8/12-Collections.md)
  * [集合与迭代器](/book/on-java8/12-Collections.md)
  * [for-in和迭代器](/book/on-java8/12-Collections.md)
  * [本章小结](/book/on-java8/12-Collections.md)
  
## 第十三章 函数式编程
  
  * [第十三章 函数式编程](/book/on-java8/13-Functional-Programming.md)
  * [新旧对比](/book/on-java8/13-Functional-Programming.md)
  * [Lambda表达式](/book/on-java8/13-Functional-Programming.md)
  * [方法引用](/book/on-java8/13-Functional-Programming.md)
  * [函数式接口](/book/on-java8/13-Functional-Programming.md)
  * [高阶函数](/book/on-java8/13-Functional-Programming.md)
  * [闭包](/book/on-java8/13-Functional-Programming.md)
  * [函数组合](/book/on-java8/13-Functional-Programming.md)
  * [柯里化和部分求值](/book/on-java8/13-Functional-Programming.md)
  * [纯函数式编程](/book/on-java8/13-Functional-Programming.md)
  * [本章小结](/book/on-java8/13-Functional-Programming.md)
  
## 第十四章 流式编程

  * [第十四章 流式编程](/book/on-java8/14-Streams.md)
  * [流支持](/book/on-java8/14-Streams.md)
  * [流创建](/book/on-java8/14-Streams.md)
  * [中级流操作](/book/on-java8/14-Streams.md)
  * [Optional类](/book/on-java8/14-Streams.md)
  * [终端操作](/book/on-java8/14-Streams.md)
  * [本章小结](/book/on-java8/14-Streams.md)
  
## 第十五章 异常
  
  * [第十五章 异常](/book/on-java8/15-Exceptions.md)
  * [异常概念](/book/on-java8/15-Exceptions.md)
  * [基本异常](/book/on-java8/15-Exceptions.md)
  * [异常捕获](/book/on-java8/15-Exceptions.md)
  * [自定义异常](/book/on-java8/15-Exceptions.md)
  * [异常规范](/book/on-java8/15-Exceptions.md)
  * [任意异常捕获](/book/on-java8/15-Exceptions.md)
  * [Java标准异常](/book/on-java8/15-Exceptions.md)
  * [finally关键字](/book/on-java8/15-Exceptions.md)
  * [异常限制](/book/on-java8/15-Exceptions.md)
  * [异常构造](/book/on-java8/15-Exceptions.md)
  * [Try-With-Resources用法](/book/on-java8/15-Exceptions.md)
  * [异常匹配](/book/on-java8/15-Exceptions.md)
  * [异常准则](/book/on-java8/15-Exceptions.md)
  * [异常指南](/book/on-java8/15-Exceptions.md)
  * [本章小结](/book/on-java8/15-Exceptions.md)
  
## 第十六章 代码校验

  * [第十六章 代码校验](/book/on-java8/16-Validating-Your-Code.md)
  * [测试](/book/on-java8/16-Validating-Your-Code.md)
  * [前提条件](/book/on-java8/16-Validating-Your-Code.md)
  * [测试驱动开发](/book/on-java8/16-Validating-Your-Code.md)
  * [日志](/book/on-java8/16-Validating-Your-Code.md)
  * [调试](/book/on-java8/16-Validating-Your-Code.md)
  * [基准测试](/book/on-java8/16-Validating-Your-Code.md)
  * [分析和优化](/book/on-java8/16-Validating-Your-Code.md)
  * [风格检测](/book/on-java8/16-Validating-Your-Code.md)
  * [静态错误分析](/book/on-java8/16-Validating-Your-Code.md)
  * [代码重审](/book/on-java8/16-Validating-Your-Code.md)
  * [结对编程](/book/on-java8/16-Validating-Your-Code.md)
  * [重构](/book/on-java8/16-Validating-Your-Code.md)
  * [持续集成](/book/on-java8/16-Validating-Your-Code.md)
  * [本章小结](/book/on-java8/16-Validating-Your-Code.md)
  
## 第十七章 文件
  
  * [第十七章 文件](/book/on-java8/17-Files.md)
  * [文件和目录路径](/book/on-java8/17-Files.md)
  * [目录](/book/on-java8/17-Files.md)
  * [文件系统](/book/on-java8/17-Files.md)
  * [路径监听](/book/on-java8/17-Files.md)
  * [文件查找](/book/on-java8/17-Files.md)
  * [文件读写](/book/on-java8/17-Files.md)
  * [本章小结](/book/on-java8/17-Files.md)
  
## 第十八章 字符串

  * [第十八章 字符串](/book/on-java8/18-Strings.md)
  * [字符串的不可变](/book/on-java8/18-Strings.md)
  * [重载和StringBuilder](/book/on-java8/18-Strings.md)
  * [意外递归](/book/on-java8/18-Strings.md)
  * [字符串操作](/book/on-java8/18-Strings.md)
  * [格式化输出](/book/on-java8/18-Strings.md)
  * [常规表达式](/book/on-java8/18-Strings.md)
  * [扫描输入](/book/on-java8/18-Strings.md)
  * [StringTokenizer类](/book/on-java8/18-Strings.md)
  * [本章小结](/book/on-java8/18-Strings.md)
  
## 第十九章 类型信息
  
  * [第十九章 类型信息](/book/on-java8/19-Type-Information.md)
  * [运行时类型信息](/book/on-java8/19-Type-Information.md)
  * [类的对象](/book/on-java8/19-Type-Information.md)
  * [类型转换检测](/book/on-java8/19-Type-Information.md)
  * [注册工厂](/book/on-java8/19-Type-Information.md)
  * [类的等价比较](/book/on-java8/19-Type-Information.md)
  * [反射运行时类信息](/book/on-java8/19-Type-Information.md)
  * [动态代理](/book/on-java8/19-Type-Information.md)
  * [Optional类](/book/on-java8/19-Type-Information.md)
  * [接口和类型](/book/on-java8/19-Type-Information.md)
  * [本章小结](/book/on-java8/19-Type-Information.md)
  
## 第二十章 泛型 

  * [第二十章 泛型](/book/on-java8/20-Generics.md)
  * [简单泛型](/book/on-java8/20-Generics.md)
  * [泛型接口](/book/on-java8/20-Generics.md)
  * [泛型方法](/book/on-java8/20-Generics.md)
  * [复杂模型构建](/book/on-java8/20-Generics.md)
  * [泛型擦除](/book/on-java8/20-Generics.md)
  * [补偿擦除](/book/on-java8/20-Generics.md)
  * [边界](/book/on-java8/20-Generics.md)
  * [通配符](/book/on-java8/20-Generics.md)
  * [问题](/book/on-java8/20-Generics.md)
  * [自我约束类型](/book/on-java8/20-Generics.md)
  * [动态类型安全](/book/on-java8/20-Generics.md)
  * [泛型异常](/book/on-java8/20-Generics.md)
  * [混入](/book/on-java8/20-Generics.md)
  * [潜在类型](/book/on-java8/20-Generics.md)
  * [补偿不足](/book/on-java8/20-Generics.md)
  * [辅助潜在类型](/book/on-java8/20-Generics.md)
  * [泛型的优劣](/book/on-java8/20-Generics.md)
  
## 第二十一章 数组

  * [第二十一章 数组](/book/on-java8/21-Arrays.md)
  * [数组特性](/book/on-java8/21-Arrays.md)
  * [一等对象](/book/on-java8/21-Arrays.md)
  * [返回数组](/book/on-java8/21-Arrays.md)
  * [多维数组](/book/on-java8/21-Arrays.md)
  * [泛型数组](/book/on-java8/21-Arrays.md)
  * [Arrays的fill方法](/book/on-java8/21-Arrays.md)
  * [Arrays的setAll方法](/book/on-java8/21-Arrays.md)
  * [增量生成](/book/on-java8/21-Arrays.md)
  * [随机生成](/book/on-java8/21-Arrays.md)
  * [泛型和基本数组](/book/on-java8/21-Arrays.md)
  * [数组元素修改](/book/on-java8/21-Arrays.md)
  * [数组并行](/book/on-java8/21-Arrays.md)
  * [Arrays工具类](/book/on-java8/21-Arrays.md)
  * [数组拷贝](/book/on-java8/21-Arrays.md)
  * [数组比较](/book/on-java8/21-Arrays.md)
  * [流和数组](/book/on-java8/21-Arrays.md)
  * [数组排序](/book/on-java8/21-Arrays.md)
  * [binarySearch二分查找](/book/on-java8/21-Arrays.md)
  * [parallelPrefix并行前缀](/book/on-java8/21-Arrays.md)
  * [本章小结](/book/on-java8/21-Arrays.md)
  
## 第二十二章 枚举

  * [第二十二章 枚举](/book/on-java8/22-Enumerations.md)
  * [基本功能](/book/on-java8/22-Enumerations.md)
  * [方法添加](/book/on-java8/22-Enumerations.md)
  * [switch语句](/book/on-java8/22-Enumerations.md)
  * [values方法](/book/on-java8/22-Enumerations.md)
  * [实现而非继承](/book/on-java8/22-Enumerations.md)
  * [随机选择](/book/on-java8/22-Enumerations.md)
  * [使用接口组织](/book/on-java8/22-Enumerations.md)
  * [使用EnumSet替代Flags](/book/on-java8/22-Enumerations.md)
  * [使用EnumMap](/book/on-java8/22-Enumerations.md)
  * [常量特定方法](/book/on-java8/22-Enumerations.md)
  * [多次调度](/book/on-java8/22-Enumerations.md)
  * [本章小结](/book/on-java8/22-Enumerations.md)
  
## 第二十三章 注解

  * [第二十三章 注解](/book/on-java8/23-Annotations.md)
  * [基本语法](/book/on-java8/23-Annotations.md)
  * [编写注解处理器](/book/on-java8/23-Annotations.md)
  * [使用javac处理注解](/book/on-java8/23-Annotations.md)
  * [基于注解的单元测试](/book/on-java8/23-Annotations.md)
  * [本章小结](/book/on-java8/23-Annotations.md)
  
## 第二十四章 并发编程

  * [第二十四章 并发编程](/book/on-java8/24-Concurrent-Programming.md)
  * [术语问题](/book/on-java8/24-Concurrent-Programming.md)
  * [并发的超能力](/book/on-java8/24-Concurrent-Programming.md)
  * [针对速度](/book/on-java8/24-Concurrent-Programming.md)
  * [四句格言](/book/on-java8/24-Concurrent-Programming.md)
  * [残酷的真相](/book/on-java8/24-Concurrent-Programming.md)
  * [本章其余部分](/book/on-java8/24-Concurrent-Programming.md)
  * [并行流](/book/on-java8/24-Concurrent-Programming.md)
  * [创建和运行任务](/book/on-java8/24-Concurrent-Programming.md)
  * [终止耗时任务](/book/on-java8/24-Concurrent-Programming.md)
  * [CompletableFuture类](/book/on-java8/24-Concurrent-Programming.md)
  * [死锁](/book/on-java8/24-Concurrent-Programming.md)
  * [构造函数非线程安全](/book/on-java8/24-Concurrent-Programming.md)
  * [复杂性和代价](/book/on-java8/24-Concurrent-Programming.md)
  * [本章小结](/book/on-java8/24-Concurrent-Programming.md)
  
## 第二十五章 设计模式 

  * [第二十五章 设计模式](/book/on-java8/25-Patterns.md)
  * [概念](/book/on-java8/25-Patterns.md)
  * [构建型](/book/on-java8/25-Patterns.md)
  * [面向实施](/book/on-java8/25-Patterns.md)
  * [工厂模式](/book/on-java8/25-Patterns.md)
  * [函数对象](/book/on-java8/25-Patterns.md)
  * [接口改变](/book/on-java8/25-Patterns.md)
  * [解释器](/book/on-java8/25-Patterns.md)
  * [回调](/book/on-java8/25-Patterns.md)
  * [多次调度](/book/on-java8/25-Patterns.md)
  * [模式重构](/book/on-java8/25-Patterns.md)
  * [抽象用法](/book/on-java8/25-Patterns.md)
  * [多次派遣](/book/on-java8/25-Patterns.md)
  * [访问者模式](/book/on-java8/25-Patterns.md)
  * [RTTI的优劣](/book/on-java8/25-Patterns.md)
  * [本章小结](/book/on-java8/25-Patterns.md)

## 附录:补充
  
  * [附录:补充](/book/on-java8/Appendix-Supplements.md)
  * [可下载的补充](/book/on-java8/Appendix-Supplements.md)
  * [通过Thinking-in-C来巩固Java基础](/book/on-java8/Appendix-Supplements.md)
  * [动手实践](/book/on-java8/Appendix-Supplements.md)
  
## 附录:编程指南

  * [附录:编程指南](/book/on-java8/Appendix-Programming-Guidelines.md)
  * [设计](/book/on-java8/Appendix-Programming-Guidelines.md)
  * [实现](/book/on-java8/Appendix-Programming-Guidelines.md)

## [附录:文档注释](/book/on-java8/Appendix-Javadoc.md)
## 附录:对象传递和返回

  * [附录:对象传递和返回](/book/on-java8/Appendix-Passing-and-Returning-Objects.md)
  * [传递引用](/book/on-java8/Appendix-Passing-and-Returning-Objects.md)
  * [本地拷贝](/book/on-java8/Appendix-Passing-and-Returning-Objects.md)
  * [控制克隆](/book/on-java8/Appendix-Passing-and-Returning-Objects.md)
  * [不可变类](/book/on-java8/Appendix-Passing-and-Returning-Objects.md)
  * [本章小结](/book/on-java8/Appendix-Passing-and-Returning-Objects.md)
  
## 附录:流式IO

  * [附录:流式IO](/book/on-java8/Appendix-IO-Streams.md)
  * [输入流类型](/book/on-java8/Appendix-IO-Streams.md)
  * [输出流类型](/book/on-java8/Appendix-IO-Streams.md)
  * [添加属性和有用的接口](/book/on-java8/Appendix-IO-Streams.md)
  * [Reader和Writer](/book/on-java8/Appendix-IO-Streams.md)
  * [RandomAccessFile类](/book/on-java8/Appendix-IO-Streams.md)
  * [IO流典型用途](/book/on-java8/Appendix-IO-Streams.md)
  * [本章小结](/book/on-java8/Appendix-IO-Streams.md)
  
## 附录:标准IO

  * [附录:标准IO](/book/on-java8/Appendix-Standard-IO.md)
  * [执行控制](/book/on-java8/Appendix-Standard-IO.md)
  
## 附录:新IO

  * [附录:新IO](/book/on-java8/Appendix-New-IO.md)
  * [ByteBuffer](/book/on-java8/Appendix-New-IO.md)
  * [转换数据](/book/on-java8/Appendix-New-IO.md)
  * [获取原始类型](/book/on-java8/Appendix-New-IO.md)
  * [视图缓冲区](/book/on-java8/Appendix-New-IO.md)
  * [使用缓冲区进行数据操作](/book/on-java8/Appendix-New-IO.md)
  * [内存映射文件](/book/on-java8/Appendix-New-IO.md)
  * [文件锁定](/book/on-java8/Appendix-New-IO.md)
  
## 附录:理解equals和hashCode方法

  * [附录:理解equals和hashCode方法](/book/on-java8/Appendix-Understanding-equals-and-hashCode.md)
  * [equals典范](/book/on-java8/Appendix-Understanding-equals-and-hashCode.md)
  * [哈希和哈希码](/book/on-java8/Appendix-Understanding-equals-and-hashCode.md)
  * [调整HashMap](/book/on-java8/Appendix-Understanding-equals-and-hashCode.md)
  
## 附录:集合主题

  * [附录:集合主题](/book/on-java8/Appendix-Collection-Topics.md)
  * [示例数据](/book/on-java8/Appendix-Collection-Topics.md)
  * [List表现](/book/on-java8/Appendix-Collection-Topics.md)
  * [Set表现](/book/on-java8/Appendix-Collection-Topics.md)
  * [在Map中使用函数式操作](/book/on-java8/Appendix-Collection-Topics.md)
  * [选择Map的部分](/book/on-java8/Appendix-Collection-Topics.md)
  * [集合的fill方法](/book/on-java8/Appendix-Collection-Topics.md)
  * [使用Flyweight自定义集合和Map](/book/on-java8/Appendix-Collection-Topics.md)
  * [集合功能](/book/on-java8/Appendix-Collection-Topics.md)
  * [可选操作](/book/on-java8/Appendix-Collection-Topics.md)
  * [Set和存储顺序](/book/on-java8/Appendix-Collection-Topics.md)
  * [队列](/book/on-java8/Appendix-Collection-Topics.md)
  * [理解Map](/book/on-java8/Appendix-Collection-Topics.md)
  * [集合工具类](/book/on-java8/Appendix-Collection-Topics.md)
  * [持有引用](/book/on-java8/Appendix-Collection-Topics.md)
  * [避免旧式类库](/book/on-java8/Appendix-Collection-Topics.md)
  * [本章小结](/book/on-java8/Appendix-Collection-Topics.md)
  
## 附录:并发底层原理

  * [附录:并发底层原理](/book/on-java8/Appendix-Low-Level-Concurrency.md)
  * [线程](/book/on-java8/Appendix-Low-Level-Concurrency.md)
  * [异常捕获](/book/on-java8/Appendix-Low-Level-Concurrency.md)
  * [资源共享](/book/on-java8/Appendix-Low-Level-Concurrency.md)
  * [volatile关键字](/book/on-java8/Appendix-Low-Level-Concurrency.md)
  * [原子性](/book/on-java8/Appendix-Low-Level-Concurrency.md)
  * [关键部分](/book/on-java8/Appendix-Low-Level-Concurrency.md)
  * [库组件](/book/on-java8/Appendix-Low-Level-Concurrency.md)
  * [本章小结](/book/on-java8/Appendix-Low-Level-Concurrency.md)
  
## 附录:数据压缩

  * [附录:数据压缩](/book/on-java8/Appendix-Data-Compression.md)
  * [使用Gzip简单压缩](/book/on-java8/Appendix-Data-Compression.md)
  * [使用zip多文件存储](/book/on-java8/Appendix-Data-Compression.md)
  * [Java的jar](/book/on-java8/Appendix-Data-Compression.md)
  
## 附录:对象序列化

  * [附录:对象序列化](/book/on-java8/Appendix-Object-Serialization.md)
  * [查找类](/book/on-java8/Appendix-Object-Serialization.md)
  * [控制序列化](/book/on-java8/Appendix-Object-Serialization.md)
  * [使用持久化](/book/on-java8/Appendix-Object-Serialization.md)
  
## 附录:静态语言类型检查

  * [附录:静态语言类型检查](/book/on-java8/Appendix-Benefits-and-Costs-of-Static-Type-Checking.md)
  * [前言](/book/on-java8/Appendix-Benefits-and-Costs-of-Static-Type-Checking.md)
  * [静态类型检查和测试](/book/on-java8/Appendix-Benefits-and-Costs-of-Static-Type-Checking.md)
  * [如何提升打字](/book/on-java8/Appendix-Benefits-and-Costs-of-Static-Type-Checking.md)
  * [生产力的成本](/book/on-java8/Appendix-Benefits-and-Costs-of-Static-Type-Checking.md)
  * [静态和动态](/book/on-java8/Appendix-Benefits-and-Costs-of-Static-Type-Checking.md)

## [附录:C++和Java的优良传统](/book/on-java8/Appendix-The-Positive-Legacy-of-C-plus-plus-and-Java.md)
## 附录:成为一名程序员

  * [附录:成为一名程序员](/book/on-java8/Appendix-Becoming-a-Programmer.md)
  * [如何开始](/book/on-java8/Appendix-Becoming-a-Programmer.md)
  * [码农生涯](/book/on-java8/Appendix-Becoming-a-Programmer.md)
  * [百分之五的神话](/book/on-java8/Appendix-Becoming-a-Programmer.md)
  * [重在动手](/book/on-java8/Appendix-Becoming-a-Programmer.md)
  * [像打字般编程](/book/on-java8/Appendix-Becoming-a-Programmer.md)
  * [做你喜欢的事](/book/on-java8/Appendix-Becoming-a-Programmer.md)

## [词汇表](/book/on-java8/GLOSSARY.md)
