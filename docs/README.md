# Hello

# 后端

- [Activiti7](back/Activiti7.md)
- [Bio](back/Bio.md)
- [Nio](back/Nio.md)
- [Netty](back/Netty.md)
- [JavaHtml转PDF](back/HtmlToPdf.md)
- [Java实现C#TrippleDES加解密](back/JavaImplTrippleDES.md)
- [Java反射](back/JavaReflex.md)
- [MyBatis](back/MyBatis.md)
- [SpringMVC](back/SpringMVC.md)
- [SpringDataJpa](back/SpringDataJpa.md)
- [Zookeeper](back/Zookeeper.md)
- [FreeMarker](back/FreeMarker.md)
- [Tomcat7性能优化](back/Tomcat7性能优化.md)

## 消息中间件

- [RabbitMQ](back/RabbitMQ.md)
- [RocketMQ](back/RocketMQ.md)
- [ActiveMQ](back/ActiveMQ.md)

## 数据存储

- [MySQL](back/MySQL.md)
- [Redis](back/Redis.md)
- [MongoDB](back/MongoDB.md)
- [ElasticSearch](back/ElasticSearch.md)
- [Solr](back/Solr.md)
- [FastDFS](back/FastDFS.md)

## 微服务

- [初识SpringCloud](back/spring-cloud/InitSpringCloud.md)
- [Ribbon](back/spring-cloud/Ribbon.md)
- [Hystrix](back/spring-cloud/Hystrix.md)
- [OpenFeign](back/spring-cloud/SpringCloudOpenFeign.md)
- [Gateway](back/spring-cloud/SpringCloudGateway.md)
- [Config](back/spring-cloud/SpringCloudConfig.md)
- [Alibaba Nacos](back/spring-cloud/SpringCloudAlibabaNacos.md)
- [Alibaba Sentinel](back/spring-cloud/SpringCloudAlibabaSentinel.md)
- [Consul](back/spring-cloud/SpringCloudConsul.md)
- [Apollo](back/spring-cloud/Apollo.md)

## 书籍

- [OnJava8](main/OnJava8.md)


# 前端

- [ViteJs 创建 Vue 工程](web/ViteCreateVueProJect.md)


# 运维

- [Linux](system/linux.md)
- [Docker](system/Docker.md)
- [GitLab](system/GitLab.md)
- [Gogs](system/Gogs.md)

# 面试题

- [面试题](面试题.md)


# 书籍

- [OnJava8](/book/on-java8/OnJava8.md)
